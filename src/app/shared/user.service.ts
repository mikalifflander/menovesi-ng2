import {Injectable, OnInit} from '@angular/core';
import {AttributeService} from "../attributes/attribute.service";
import {Http} from "@angular/http";

@Injectable()
export class UserService implements OnInit{

  private static LOGIN_URL = AttributeService.API_URL + '/authentications';

  private sending: number;

  private user: any;

  constructor(private http: Http) { }

  ngOnInit() {
    this.sending = 0;
  }

  doLogin(credentials) {

    this.sending ++;

    var obs = this
        .http
        .post(UserService.LOGIN_URL, credentials)
        .map(response => response.json())
        .share();

    obs.subscribe(data => {
      this.sending --;
      this.user = data[0];
    }, error => {
      this.user = undefined;
      this.sending--;
      console.error('Login failed. ' + error);
    });

    return obs;
  }

  isAuthenticated() {
    return this.user != undefined;
  }

}

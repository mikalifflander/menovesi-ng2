import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {Station} from "../models/station";
import {Subject, BehaviorSubject} from "rxjs";
import {PriceList} from "../models/price-list";
import {AttributeService} from "../../attributes/attribute.service";
import {Price} from "../models/price";

@Injectable()
export class StationManagerService {

  private static STATION_URL = AttributeService.API_URL + "/stations";
  private static  PRICE_PATH = '/prices';

  private _stations$: Subject<Station[]>;
  private _prices$: Subject<PriceList>;
  private _selected$: BehaviorSubject<Station>;

  private loading: number;
  private sending: number;

  private _datastore : {
    stations: Station[],
    prices: PriceList,
    selected: Station
  };

  constructor(private http: Http) {
    this._datastore = { stations: [], prices: new PriceList(), selected: null};

    this._stations$ = <Subject<Station[]>> new Subject();
    this._prices$ = <Subject<PriceList>> new Subject();
    this._selected$ = new BehaviorSubject<Station>(new Station());

    this.loading = this.sending = 0;
  }

  // =======================================
  // Observable getters
  // =======================================
  get stations$() {
    return this._stations$.asObservable();
  }

  get selected$() {
    return this._selected$.asObservable();
  }

  get selected() {
    return this._datastore.selected;
  }

  get prices$() {
    return this._prices$.asObservable();
  }

  get datastore() {
    return this._datastore;
  }
  // =======================================
  // public API
  // =======================================

  create(station: Station) {
    console.log('create station called');

    this.sending++;
    var obs =
        this.http.post(StationManagerService.STATION_URL, station)
        .map(response => {
          this.sending--;
          return response.json();
        }).share();

    obs.subscribe(data => {
      if(data.length > 0)
        this.datastore.stations.push(data[0]);
        this.datastore.stations = this.datastore.stations.sort(this.nameCompare)
      }, error => {
        console.log('Station creation failed. ' + error);
    });

    return obs;
  }

  loadStations() {
    this.loading += 1;
    this.http.get(StationManagerService.STATION_URL)
        .map(response => response.json())
        .subscribe(data => {
          this.datastore.stations = data.sort(this.nameCompare);
          this._stations$.next(this.datastore.stations);
          this.loading -= 1;
        }, error => {
          console.log('Station load failed. ' + error);
          this.loading -= 1;
        });
  }

  nameCompare(s1, s2) {
    return s1.name > s2.name
        ? 1
        : s1.name < s2.name
        ? -1
        :0;
  }

  select(station: Station) {
    this.datastore.selected = station;
    this._selected$.next(this.datastore.selected);
    if(this.datastore.selected && this.datastore.selected.id > 0)
      this.loadPrices();
  }

  updateSelected() {

    this.sending++;
    var obs = this.http.put(StationManagerService.STATION_URL + '/' + this.datastore.selected.id, this.datastore.selected)
        .map(response => {
          this.sending--;
          return response.json();
        })
        .share();

    obs.subscribe(item => {
          for(var i=0; i<this.datastore.stations.length; i++) {
            var st = this.datastore.stations[i];
            if(st.id == item.id) {
              this.datastore.stations[i] = item;
              this.select(this.datastore.stations[i]);
              return;
            }
          }
        });
    return obs;
  }

  createPrice(price: Price) {
    if(!this.datastore.selected) return;

    this.sending++;

    var obs = this.http
        .post(StationManagerService.STATION_URL  + '/' + this.datastore.selected.id + StationManagerService.PRICE_PATH, price)
        .map(response => response.json())
        .share();

      obs.subscribe(data => {
          this.sending--;
          this.loadPrices();
        }, error => {
          console.error('Save price failed. ' + error);
        });

    return obs;
  }

  isActive() {
    return (this.isLoading() || this.isSending());
  }

  isLoading() {
    return this.loading > 0;
  }

  isSending() {
    return this.sending > 0;
  }

  isSelected() {
    return (this.datastore.selected && this.datastore.selected.id > 0);
  }

  isGeolocated() {
    if(!this.isSelected()) return false;

    if(this.datastore.selected.longitude == null || this.datastore.selected.latitude == null)
      return false;

    if(isNaN(this.datastore.selected.latitude) || isNaN(this.datastore.selected.longitude))
      return false;

    if(this.datastore.selected.latitude == 0 && this.datastore.selected.longitude == 0)
      return false;

    return true;
  }
  // =======================================
  // private helpers
  // =======================================
  private loadPrices() {
    if(!this.datastore.selected) {
      return;
    }

    this.loading++;

    this.http
        .get(StationManagerService.STATION_URL + '/' + this.datastore.selected.id + StationManagerService.PRICE_PATH + '?future=true')
        .map(response => {
          this.loading--;
          if(response.status == 200)
            return response.json();
          return [];
        }, error => {
          console.log('Price load failed: '+ error);
          this.loading--;
        })
        .subscribe(data => {
          this.datastore.prices = data;
          this._prices$.next(this.datastore.prices);
        }, error => console.log('Price load failed'));
  }
}

import { Component, OnInit } from '@angular/core';
import {AttributeService} from "../../attributes/attribute.service";
import {StationManagerService} from "../service/station-manager.service";
import {Price} from "../models/price";
import {Attributes} from "../../attributes/Attributes";
import {NgbTimeStruct, NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'station-price-form',
  templateUrl: './station-price-form.component.html',
  styleUrls: ['./station-price-form.component.css']
})
export class StationPriceFormComponent implements OnInit {

  private price: Price;
  private attributes: Attributes;

  private selectedDate: NgbDateStruct;
  private selectedTime: NgbTimeStruct;

  private message: {
    msg: string,
    class: string
  }

  constructor(private service: StationManagerService, private attrService: AttributeService) { }

  ngOnInit() {
    this.price = new Price();
    this.attributes = this.attrService.attributes;


    this.selectedTime = {
      hour: this.price.validFrom.getHours(),
      minute:  this.price.validFrom.getMinutes(),
      second: 0
    }

    this.selectedDate = {
      day: this.price.validFrom.getDate(),
      month: this.price.validFrom.getMonth()+1,
      year: this.price.validFrom.getFullYear()
    }

  }

  private get validFrom() : Date {
    return new Date(this.selectedDate.year, this.selectedDate.month-1, this.selectedDate.day, this.selectedTime.hour , this.selectedTime.minute, this.selectedTime.second, 0);
  }

  save() {
    this.price.validFrom = this.validFrom;
    this.service.createPrice(this.price)
        .subscribe(data => {
          this.price = new Price();
          this.showSuccess('Price saved successfully');
        }, (error) => {
          this.showError('Save failed. ' + error);
        });

  }

  showSuccess(msg) {
    this.message = {
      msg: msg,
      class: 'success'
    }

    setTimeout(() => {
      this.message = null;
    }, 5000);
  }

  showError(msg) {
    this.message = {
      msg: msg,
      class: 'alert'
    }

    setTimeout(() => {
      this.message = null;
    }, 5000);
  }
}

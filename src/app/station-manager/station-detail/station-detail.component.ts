import { Component, OnInit } from '@angular/core';
import {Station} from "../models/station";
import {StationManagerService} from "../service/station-manager.service";
import {AttributeService} from "../../attributes/attribute.service";

import {Attributes} from "../../attributes/Attributes";
import {LatLngLiteral} from "angular2-google-maps/core";
import {StationDetailModalComponent} from "../station-detail-modal/station-detail-modal.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {StationPriceModalComponent} from "../station-price-modal/station-price-modal.component";

@Component({
  selector: 'station-detail',
  templateUrl: './station-detail.component.html',
  styleUrls: ['./station-detail.component.css'],
})
export class StationDetailComponent implements OnInit {

  private static MAP_ZOOM = 13;
  private station: Station;

  private attributes: Attributes;

  private lng: number = 23.1156;
  private lat: number = 61.4456;
  private zoom: number = StationDetailComponent.MAP_ZOOM;

  constructor(private service: StationManagerService,
              private attrService: AttributeService,
              private modalService: NgbModal) { }

  ngOnInit() {

    this.newStation();

    this.service.selected$.subscribe(
        (item: Station) => {
          this.station = item;
          this.lat = Number(this.station.latitude);
          this.lng = Number(this.station.longitude);
          this.zoom = StationDetailComponent.MAP_ZOOM;
        });

    this.attributes = this.attrService.attributes;

  }

  ngOnDestroy() {
  }

  newStation() {
    this.station = new Station();
    this.station.contact = 'demo@menovesi.fi';
  }

  openModal() {
    this.modalService.open(StationDetailModalComponent);
  }

  openPriceModal() {
    this.modalService.open(StationPriceModalComponent);
  }
}

/**
 * Created by mika on 08/10/2016.
 */
export class Price {
    public id: number = 0;
    public type: string = '';
    public pricePerUnit: number = 0.000;
    public validFrom: Date = new Date();
    public description: string = '';
    public fueltype_id: number = 0;
}
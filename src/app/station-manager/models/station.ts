export class Station {
    public id: number = 0;
    public name: string;
    public brand: string;
    public brand_id: number = 0;
    public company_id: number = 0;
    public company: string;
    public type: string;
    public stationtype_id: number = 0;
    public streetaddress: string ;
    public zipcode:  number;
    public city: string;
    public state: string;
    public country: string;
    public contact: string;
    public longitude:  number;
    public latitude:  number;
    public manager_id: number = 2;

    public isGeolocated() {
        if( isNaN(this.latitude) || isNaN(this.longitude) )
            return false;
        return true;
    }
}
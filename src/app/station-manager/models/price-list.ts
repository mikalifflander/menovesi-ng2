import {Price} from "./price";
/**
 * Created by mika on 09/10/2016.
 */

export class PriceList {
    public current: Price[];
    public history: Price[];
    public future: Price[];
}
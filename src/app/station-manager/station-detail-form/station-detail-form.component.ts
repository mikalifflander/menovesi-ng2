import { Component, OnInit } from '@angular/core';
import {Station} from "../models/station";
import {StationManagerService} from "../service/station-manager.service";
import {Attributes} from "../../attributes/Attributes";
import {AttributeService} from "../../attributes/attribute.service";

@Component({
  selector: 'station-detail-form',
  templateUrl: './station-detail-form.component.html',
  styleUrls: ['./station-detail-form.component.css']
})
export class StationDetailFormComponent implements OnInit {

  private station: Station;
  private attributes: Attributes;

  private message: {
    msg: string,
    class: string
  };

  constructor(private service: StationManagerService, private attrService: AttributeService) { }

  ngOnInit() {
    this.station = this.service.datastore.selected || new Station();
    this.attributes = this.attrService.attributes;
  }

  createOrUpdate() {
    if(this.service.isSelected())
      this.service.updateSelected()
          .subscribe(data => {
            this.showSuccess('Station updated successfully.');
          }, (error) => {
            this.showError('Update failed. ' + error);
          });
    else {
      this.service.create(this.station)
          .subscribe(data => {
            this.showSuccess('Station created successfully');
          }, error => {
            this.showError('Station creation failed. ' + error);
          })
    }
  }

  showSuccess(msg) {
    this.message = {
      msg: msg,
      class: 'success'
    };

    setTimeout(() => {
      this.message = null;
    }, 5000)
  }

  showError(msg) {
    this.message = {
      msg: msg,
      class: 'alert'
    }

    setTimeout(() => {
      this.message = null;
    }, 5000)
  }
}

import { Component, OnInit } from '@angular/core';
import {StationManagerService} from "../service/station-manager.service";
import {Observable} from "rxjs";
import {Station} from "../models/station";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {StationDetailModalComponent} from "../station-detail-modal/station-detail-modal.component";

@Component({
  selector: 'station-list',
  templateUrl: './station-list.component.html',
  styleUrls: ['./station-list.component.css']
})
export class StationListComponent implements OnInit {

  private stations$: Observable<Station[]>;


  constructor(private service: StationManagerService, private modalService: NgbModal) { }

  ngOnInit() {
    this.stations$ = this.service.stations$;

    this.service.loadStations();
  }

  private selectStation(station: Station) {
    this.service.select(station);
  }

  openNewStationModal() {
    this.service.select(new Station());
    this.modalService.open(StationDetailModalComponent);
  }
}

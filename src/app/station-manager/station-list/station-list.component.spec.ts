/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { StationListComponent } from './station-list.component';
import {StationManagerService} from "../service/station-manager.service";

describe('Component: StationList', () => {
  it('should create an instance', () => {
    let component = new StationListComponent(StationManagerService);
    expect(component).toBeTruthy();
  });
});

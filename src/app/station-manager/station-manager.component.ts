import { Component, OnInit } from '@angular/core';
import {StationManagerService} from "./service/station-manager.service";

@Component({
  selector: 'app-station-manager',
  templateUrl: './station-manager.component.html',
  styleUrls: ['./station-manager.component.css']
})
export class StationManagerComponent implements OnInit {

  constructor(private service: StationManagerService) { }

  ngOnInit() {
  }

}

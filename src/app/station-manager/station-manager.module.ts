import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";

import { StationManagerComponent } from './station-manager.component';
import {StationManagerService} from "./service/station-manager.service";
import { StationListComponent } from './station-list/station-list.component';
import { StationDetailComponent } from './station-detail/station-detail.component';
import { StationPriceComponent } from './station-price/station-price.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { StationDetailModalComponent } from './station-detail-modal/station-detail-modal.component';
import { StationDetailFormComponent } from './station-detail-form/station-detail-form.component';
import { StationPriceFormComponent } from './station-price-form/station-price-form.component';
import { StationPriceModalComponent } from './station-price-modal/station-price-modal.component';
import {AgmCoreModule} from "angular2-google-maps/core";



@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule.forRoot(),
        AgmCoreModule
    ],
    declarations: [StationManagerComponent, StationListComponent, StationDetailComponent, StationPriceComponent, StationDetailModalComponent, StationDetailFormComponent, StationPriceFormComponent, StationPriceModalComponent],
    providers: [StationManagerService],
    entryComponents: [StationDetailModalComponent, StationPriceModalComponent]
})
export class StationManagerModule { }

import { Component, OnInit } from '@angular/core';
import {NgbModal, NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {StationManagerService} from "../service/station-manager.service";
import {Station} from "../models/station";

@Component({
  selector: 'station-detail-modal',
  templateUrl: './station-detail-modal.component.html',
  styleUrls: ['./station-detail-modal.component.css'],
})
export class StationDetailModalComponent implements OnInit {

  private station: Station;

  constructor(public activeModal: NgbActiveModal, private service: StationManagerService) { }

  ngOnInit() {
  }

}

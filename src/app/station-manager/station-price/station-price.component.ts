import { Component, OnInit } from '@angular/core';
import {StationManagerService} from "../service/station-manager.service";
import {Observable} from "rxjs";
import {PriceList} from "../models/price-list";
import {Price} from "../models/price";
import {Attributes} from "../../attributes/Attributes";
import {AttributeService} from "../../attributes/attribute.service";

import { NgbDateStruct, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'station-price',
  templateUrl: './station-price.component.html',
  styleUrls: ['./station-price.component.css']
})
export class StationPriceComponent implements OnInit {

    private prices$: Observable<PriceList>;

    private attributes: Attributes;

    private prices: PriceList;
    private price : Price;

    constructor(private service: StationManagerService, private attrService: AttributeService) {
        this.prices = new PriceList();
    }

    ngOnInit() {

        this.attributes = this.attrService.attributes;

        this.price = new Price();


        this.prices$ = this.service.prices$;
        this.service.prices$.subscribe(
            (item) => {
                this.prices = item;
        },
        error => console.log('Prices stream error. ' + error));
    }

}

/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { StationPriceComponent } from './station-price.component';
import {StationManagerService} from "../service/station-manager.service";

describe('Component: StationPrice', () => {
  it('should create an instance', () => {
    let component = new StationPriceComponent(StationManagerService);
    expect(component).toBeTruthy();
  });
});

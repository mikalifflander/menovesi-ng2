import { Component, OnInit } from '@angular/core';
import {NgbModal, NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {StationManagerService} from "../service/station-manager.service";
import {AttributeService} from "../../attributes/attribute.service";
import {Attributes} from "../../attributes/Attributes";
import {Price} from "../models/price";

@Component({
  selector: 'station-price-modal',
  templateUrl: './station-price-modal.component.html',
  styleUrls: ['./station-price-modal.component.css']
})
export class StationPriceModalComponent implements OnInit {

  private attributes: Attributes;
  private price: Price;

  constructor(private activeModal: NgbActiveModal,
              private service: StationManagerService,
              private attrService: AttributeService) { }

  ngOnInit() {
    this.attributes = this.attrService.attributes;
    this.price = new Price();
  }

}

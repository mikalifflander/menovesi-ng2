import { Component, OnInit } from '@angular/core';
import {UserService} from "../shared/user.service";
import {UIRouter} from "ui-router-ng2";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    private credentials = {
        email: undefined,
        password: undefined
    };

    private message: any;

    constructor(private userService: UserService, private uiRouter: UIRouter) { }

    ngOnInit() {
        this.credentials.email = 'demo@menovesi.fi';
        this.credentials.password = 'demo';
    }

    submitLogin() {
        if(!this.credentials.email || !this.credentials.password ) return;

        if(this.credentials.email.length <= 0 || this.credentials.password.length <= 0) return;

        this.userService
            .doLogin(this.credentials)
            .subscribe(data => {
                this.uiRouter.stateService.go('stationManager');
            }, error => {
                this.message = {
                    msg: 'Login failed',
                    class: 'danger'
                };

                setTimeout(() => {
                    this.message = null;
                }, 5000)
            });
    }
}

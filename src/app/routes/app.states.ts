import {Ng2StateDeclaration} from 'ui-router-ng2';
import {WelcomeComponent} from "../welcome/welcome.component";
import {StationManagerComponent} from "../station-manager/station-manager.component";
import {LoginComponent} from "../login/login.component";

export let APP_STATES: Ng2StateDeclaration[] = [

    // Default aka the home state
    { name: 'home', component: WelcomeComponent },

    // Login
    { name: 'login', component: LoginComponent },

    // Station Manager
    { name: 'stationManager', component: StationManagerComponent}
];
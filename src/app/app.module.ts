import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { UIRouterModule} from 'ui-router-ng2';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import {APP_STATES} from "./routes/app.states";
import {AppRouterConfig} from "./routes/router.config";
import { WelcomeComponent } from './welcome/welcome.component';
import {StationManagerModule} from "./station-manager/station-manager.module";
import {AttributeService} from "./attributes/attribute.service";
import {AgmCoreModule} from "angular2-google-maps/core";
import {WelcomeModule} from "./welcome/welcome.module";
import { LoginComponent } from './login/login.component';
import {UserService} from "./shared/user.service";

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        LoginComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        UIRouterModule.forRoot({
            states: APP_STATES,
            useHash: true,
            configClass: AppRouterConfig,
            otherwise: { state: 'home', params:{} }
        }),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyCbs0l_jbbOow7uI_ANXuwzSeHkUoH2B64'
        }),
        StationManagerModule,
        WelcomeModule
    ],
    providers: [AttributeService, UserService],
    bootstrap: [AppComponent]
    })
export class AppModule { }

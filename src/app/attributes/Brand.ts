/**
 * Created by mika on 10/10/2016.
 */
export class Brand {
    public id: number;
    public name: string;
    public description: string;
}
import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {Attributes} from "./Attributes";
import {Company} from "./Company";

@Injectable()
export class AttributeService {

  private static LOCAL_URL = 'http://localhost:4000';
  private static TAMK_URL = 'http://home.tamk.fi/~c6mliffl/wap/practical/api/index.php';

  public static API_URL = AttributeService.TAMK_URL;

  private static ATTR_PATH = '/attributes';

  private _attributes: Attributes;

  constructor(private http: Http) { }


  // =============================
  // Public API
  // =============================

  get attributes(){
    return this._attributes;
  }

  companyForId(id: number) {
    for(var i=0; i<this.attributes.companies.length; i++) {
      if(this.attributes.companies[i].id == id)
        return this.attributes.companies[i];
    }
    return new Company();
  }
  loadAttributes() {
    this._attributes = new Attributes();

    var url = AttributeService.API_URL + AttributeService.ATTR_PATH;

    this.http.get(AttributeService.API_URL + AttributeService.ATTR_PATH)
        .map(response => response.json())
        .subscribe(data => {
          for(let prop in data) {
            this.attributes[prop] = data[prop];
          }
        }, error => {
          console.log('Attribute load failed. ' + error);
        });

    return this.attributes;
  }

  // =============================
  // Private Helpers
  // =============================
}

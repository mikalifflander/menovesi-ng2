import {Brand} from "./Brand";

export class Company {
    public id: number;
    public name: string;
    public brands: Brand[]
}
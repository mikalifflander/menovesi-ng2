/**
 * Created by mika on 10/10/2016.
 */

import {Company} from "./Company";
import {FuelType} from "./FuelType";
import {StationType} from "./StationType";
import {VehicleType} from "./VehicleType";

export class Attributes {
    public companies: Company[];

    public fueltypes: FuelType[];

    public stationtypes: StationType[];

    public vehicletypes: VehicleType[];

    public getBrands(companyId) {
        for(var i=0 ; i<this.companies.length; i++) {
            if(this.companies[i].id == companyId)
                return this.companies[i].brands;
        }
        return [];
    }
}
/**
 * Created by mika on 10/10/2016.
 */

export class FuelType {
    public id: number;
    public type: string;
}
/**
 * Created by mika on 10/10/2016.
 */

export class StationType {
    public id: number;
    public type: string;
}
import { Component } from '@angular/core';
import {AttributeService} from "./attributes/attribute.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private attrService: AttributeService){}

  ngOnInit() {
    this.attrService.loadAttributes();
  }
}

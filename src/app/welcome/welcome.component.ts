import { Component, OnInit } from '@angular/core';
import {WelcomeService} from "./service/welcome.service";
import {Station} from "../station-manager/models/station";

enum LocationStatus {
    UNKNOWN = 0,
    ACTIVE,
    OK,
    UNSUPPORTED,
    FAILED
}

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

    private lng: number = 23.761031799999955;
    private lat: number = 61.4972474;

    private zoom: number =  6;

    private checkLoc: string = 'Acquiring your location';
    private dots: string[] = ['','.', '..', '...'];
    private showDots: number = 0;

    private stationlist: Station[];

    LocationStatus = LocationStatus;

    private userLocationStatus: LocationStatus;

    constructor(private service: WelcomeService) { }

    get acqMessage() {
        return this.checkLoc + '' + this.dots[this.showDots];
    }

    ngOnInit() {
        if(navigator.geolocation) {
            this.userLocationStatus = LocationStatus.ACTIVE;
            this.showDotsInInterval(1500);
             navigator.geolocation.getCurrentPosition((position) => {
                this.lng = position.coords.longitude;
                this.lat = position.coords.latitude;
                this.userLocationStatus = LocationStatus.OK;
            }, (error) => {
                this.userLocationStatus = LocationStatus.FAILED;
                 this.clearAlert(5000);
            })
        } else {

            this.userLocationStatus = LocationStatus.UNSUPPORTED;
            this.clearAlert(5000);
        }

        this.stationlist = [];
        this.service.loadStations()
            .subscribe((data) => {
                for(var i=0; i<data.length; i++) {
                    if(this.service.isGeolocated(data[i]))
                        this.stationlist.push(data[i]);
                }
            }, (error) => {
                console.error('Station load failed. ' + error);
            });
    }

    showDotsInInterval(interval) {
        setInterval(() => {
            this.showDots = this.showDots < (this.dots.length - 1)
                ? this.showDots + 1
                : 0;
        }, interval)
    }

    clearAlert(timeout) {
        setTimeout(() => {
            this.userLocationStatus = LocationStatus.OK;
        }, timeout);
    }
}

/**
 * Created by mika on 16/10/2016.
 */
export class PriceInfo {
    public brand: string;
    public station: string;
    public type: string;
    public description: string;
    public pricePerUnit: number;
    public validFrom: Date;
}
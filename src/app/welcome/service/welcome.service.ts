import { Injectable } from '@angular/core';
import {AttributeService} from "../../attributes/attribute.service";
import {Attributes} from "../../attributes/Attributes";
import {Http} from "@angular/http";
import {BehaviorSubject, Subject} from "rxjs";
import {StationManagerService} from "../../station-manager/service/station-manager.service";
import {Station} from "../../station-manager/models/station";

@Injectable()
export class WelcomeService {

    private static PRICES_URL = AttributeService.API_URL + '/prices';
    private static STATION_URL = AttributeService.API_URL + '/stations';

    private _pricelist$: Subject<Object[]>;

    private _datastore: any[];

    private attributes: Attributes;

    constructor(private attrService: AttributeService, private http: Http) {
        this.attributes = this.attrService.attributes;
        this._pricelist$ = <Subject<Object[]>> new Subject();
    }

    public get pricelist$() {
        return this._pricelist$.asObservable();
    }

    loadPriceList() {
        this.http
            .get(WelcomeService.PRICES_URL)
            .map(response => response.json())
            .share()
            .subscribe(data => {

                this._datastore = [];
                var obj = {};

                for(var i=0; i<data.length; i++) {
                    var type = data[i].type;
                    if(!obj[type]) {
                        obj[type] = {type: type, items: []};
                    }
                    obj[type].items.push(data[i]);
                }

                for(let type in obj ) {
                    obj[type].items = obj[type].items.sort(this.priceSort)
                }
                for(let type in obj) {
                    this._datastore.push(obj[type]);
                }
                this._datastore = this._datastore.sort(this.fueltypeSort)
                this._pricelist$.next(this._datastore);
            }, (error) => {
                console.error('price load failed. ' + error);
            })
    }

    priceSort(p1, p2) {
        return p1.pricePerUnit > p2.pricePerUnit
            ? 1
            : p1.pricePerUnit < p2.pricePerUnit
            ? -1
            :0;
    }

    fueltypeSort(f1, f2) {
        return f1.type > f2.type
            ? 1
            : f1.type < f2.type
            ? -1
            :0;
    }

    loadStations() {
        return this.http.get(WelcomeService.STATION_URL)
            .map(response => response.json())
            .share();
    }

    isGeolocated(station: Station) {
        if(!station) return false;

        if(station.longitude == null || station.latitude == null)
            return false;

        if(isNaN(station.latitude) || isNaN(station.longitude))
            return false;

        if(station.latitude == 0 && station.longitude == 0)
            return false;

        return true;
    }

    toNumber(val: any) {
        return Number(val);
    }
}

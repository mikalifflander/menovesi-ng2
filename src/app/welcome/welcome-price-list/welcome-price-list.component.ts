import { Component, OnInit } from '@angular/core';
import {WelcomeService} from "../service/welcome.service";
import {Observable} from "rxjs";

@Component({
  selector: 'welcome-price-list',
  templateUrl: './welcome-price-list.component.html',
  styleUrls: ['./welcome-price-list.component.css']
})
export class WelcomePriceListComponent implements OnInit {

    private pricelist$: Observable<Object[]>;

    constructor(private service: WelcomeService) { }

    ngOnInit() {

        this.pricelist$ = this.service.pricelist$;

        this.service.loadPriceList();
    }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WelcomeComponent } from './welcome.component';
import { WelcomePriceListComponent } from './welcome-price-list/welcome-price-list.component';
import {WelcomeService} from "./service/welcome.service";
import {AgmCoreModule} from "angular2-google-maps/core";

@NgModule({
  imports: [
      CommonModule,
      AgmCoreModule
  ],
  declarations: [WelcomeComponent, WelcomePriceListComponent],
  providers: [WelcomeService]
})
export class WelcomeModule { }

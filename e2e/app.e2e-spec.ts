import { Ng2MenovesiPage } from './app.po';

describe('ng2-menovesi App', function() {
  let page: Ng2MenovesiPage;

  beforeEach(() => {
    page = new Ng2MenovesiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

# Menovesi.fi Web application
*a Web Application Programming practical work by Mika Lifflander*

## Live Demo
Live demo is available at [TAMK server](http://home.tamk.fi/~c6mliffl/wap/practical).

## Introduction
This application is built with [Angular 2](https://angular.io) and [Bootstrap 4 alpha](http://v4-alpha.getbootstrap.com) with dependies to 3rd party libraries [ui-router](https://ui-router.github.io) and [ng2-bootstrap](https://github.com/valor-software/ng2-bootstrap).

Project uses [angular2-cli](https://cli.angular.io) for task automation.

## Directory structure
Project is logically divided to modules and components. It provides modules **app**, **welcome**, **station-manager** and **login**. All modules are located in `src/app` directory.

Each module uses one ore more components.

Project source code structure is as follows

```
.
|-- node_modules
|--	src
|	|-- app					- application code
|	|	|-- attributes		- Service for attributes
|	|	|-- login			- Login module
|	|	|-- navbar			- Navbar component
|	|	|-- routes			- UI Router configuration
|	|	|-- shared			- Shared services
|	|	|-- station-manager	- Station manager module
|	|	|-- welcome			- Welcome module
|	|-- assests				- Images and other assets
```
 
## Author
Mika Lifflander - mika.lifflander@eng.tamk.fi

## Modules / views
### Welcome
Shows info about fuel stations and fuel prices. This is the application landing page.

### Login
Self-explanatory. Handles the login.

### Station manager
Handles station managers functionality. Add and modify stations, set fuel prices. Requires log in.
